# Exercice - Docker Compose 🐋

Vous êtes développeur et vous avez du goût pour les blondes, les brunes... On parle de bières bien sûr ! 😇​ Vous avez créé une boutique de bières en partenariat avec votre cousin il y a tout juste 1 an, et à cette occasion, vous vous dites qu'il serait temps de mettre votre boutique en avant, autrement que via les réseaux sociaux uniquement. En d'autres termes, il est temps d'entrer dans la cour des grands avec un site web dédié !  

L'objectif de cet exercice est alors de créer une stack pour le futur site de votre boutique.  

Il s'agit, pour le moment, d'une petite application permettant d'afficher une liste de bières du magasin.

L'application contient 3 endpoints :
- La liste complète des bières
- Le CA des principaux fabricants
- Les variations de ventes entre 2022 et 2023.

> Le développeur stagiaire, qui n'est là que pour 8 semaines, a débuté le développement du site et vous confirme que 'ça marche sur sa machine'.

Vous avez à votre disposition : 
- un répertoire `api` qui contient le code source d'une API Flask
- un répertoire `sql` qui contient de quoi créer une base de données MySQL contenant des données

Vous devez réaliser un fichier docker-compose qui permet de créer les conteneurs suivants :
- un conteneur MySQL
- un conteneur Flask

## Attendus ✅

- Vous devez créer les Dockerfiles nécessaires à la création des images
- Le docker-compose doit construire les images à partir des Dockerfiles

## Aides 💡

Les aides ici vous aideront à construire cette application !
Pensez à bien les lire, en complément de la documentation des images !
Précision : vous n'avez pas besoin de volumes.

### API 🌐

- Le fichier `requirements.txt` contient les dépendances python nécessaires à l'API
- Il devra être copié dans le conteneur et installé avec la commande `pip install -r requirements.txt`
- L'API doit exposer sur le port `80`.
- Le conteneur doit exposer le port `5000`
- Vous devez utiliser l'image `python` comme image de base

### BDD 💾

- Vous devez utiliser l'image `mysql` comme image de base
- Vous devez copier le fichier `beer.sql` dans l'entrypoint du conteneur MySQL
- Le nom de la base de données doit être `beer`
- Le mot de passe root doit être `root`

## Aides supplémentaires ✋

> Avant de vous diriger vers les indices, essayez de réaliser l'exercice sans :).

Si vous avez besoin d'indices supplémentaires, vous pouvez vous tourner vers le fichier suivant :
- [Aides pour le poussin de Docker](./consignes/poussin.md) 🐣